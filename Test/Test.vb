﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Factory

<TestClass()> Public Class Test

    <TestMethod()> Public Sub CrearAutoFamiliar()

        Dim auto As Auto
        auto = AutoFactory.Crear("familiar")

        Assert.AreEqual(auto.GetType, GetType(Familiar))


    End Sub


    <TestMethod()> Public Sub CrearAutoDeportivo()

        Dim auto As Auto
        auto = AutoFactory.Crear("deportivo")

        Assert.AreEqual(auto.GetType, GetType(Deportivo))


    End Sub

    <TestMethod()> Public Sub CrearAutoUtilitario()

        Dim auto As Auto
        auto = AutoFactory.Crear("utilitario")

        Assert.AreEqual(auto.GetType, GetType(Utilitario))


    End Sub

    <TestMethod()> Public Sub CrearAutoJoven()

        Dim auto As Auto
        auto = AutoFactory.Crear("joven")

        Assert.AreEqual(auto.GetType, GetType(Joven))


    End Sub

End Class