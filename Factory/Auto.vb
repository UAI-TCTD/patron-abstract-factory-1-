﻿Public Class Auto
    Protected _chasis As Chasis
    Protected _motor As Motor
    Protected _carroceria As Carroceria
    Protected _nombre As String

    Public Sub New(factoriaDeComponents As IComponenteFactory)
        _motor = factoriaDeComponents.crerMotor
        _chasis = factoriaDeComponents.crearChasis
        _carroceria = factoriaDeComponents.CrearCarroceria

    End Sub

    Public ReadOnly Property Nombre As String
        Get
            Return _nombre
        End Get
    End Property


    Public ReadOnly Property Motor As Motor
        Get
            Return _motor
        End Get
    End Property


    Public ReadOnly Property Chasis As Chasis
        Get
            Return _chasis
        End Get
    End Property

    Public ReadOnly Property Carroceria As Carroceria
        Get
            Return _carroceria
        End Get
    End Property
End Class
